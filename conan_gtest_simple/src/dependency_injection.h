#ifndef CONAN_CATCH_SIMPLE_SINGLETON_H
#define CONAN_CATCH_SIMPLE_SINGLETON_H

#include "runnable.h"

typedef struct __DependencyInjection *DependencyInjectionHandle;

/**
 * Create the class
 * @return
 */
DependencyInjectionHandle DependencyInjection_create(const Runnable* runnable);

#endif
