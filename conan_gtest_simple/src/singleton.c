#include <stddef.h>
#include "singleton.h"

// Private data
typedef struct {
    Runnable runnable;
    size_t increment;
} PrivateData;

// Add the class as a singleton to the static memory
PrivateData me = {0, 0};

static void run(Runnable* handle);

// Implement the runnable
static void run(Runnable* handle) {
    // Ignore the handle
    (void) handle;
    me.increment += 1;
}

// Add an initializer function
void Singleton_initialize() {
    // Assign the pointer. This is technically unnecessary as there will only ever be one instance of the singleton.
    me.runnable.handle = &me;
    me.runnable.run = run;
    me.increment = 0;
}

// Add a function to return the runnable
Runnable* Singleton_getRunnable() {
    return  &me.runnable;
}

size_t Singleton_getCount() {
    return me.increment;
}
