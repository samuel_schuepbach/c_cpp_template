#ifndef CONAN_CATCH_SIMPLE_RUNNABLE_H
#define CONAN_CATCH_SIMPLE_RUNNABLE_H

// Runnable interface
typedef struct __Runnable {
    // A pointer to the implementing object. Only used if the object is not a singleton
    void* handle;
    // Function to be implemented
    void (*run)(struct __Runnable* handle);
} Runnable;

typedef struct __SingletonRunnable {
    // Since there is only ever one instance of a singleton, pointers are redundant and don't have to be included.

    // Function to be implemented
    void (*run)(struct __Runnable* handle);
} SingletonRunnable;

#endif
