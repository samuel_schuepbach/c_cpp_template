#ifndef CONAN_CATCH_SIMPLE_EXAMPLE_H
#define CONAN_CATCH_SIMPLE_EXAMPLE_H

#include "runnable.h"
#include <stdio.h>

typedef struct __Class *ClassHandle;

/**
 * Create the class
 * @return
 */
ClassHandle Class_create();

/**
 * Fetch the runnable
 * @param me
 * @return
 */
Runnable *Class_getRunnable(ClassHandle me);

/**
 * Check the increment count
 * @param me
 * @return
 */
size_t Class_getCount(ClassHandle me);

#endif //CONAN_CATCH_SIMPLE_EXAMPLE_H
