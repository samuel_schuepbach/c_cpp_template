
extern "C" {
#include "example.h"
};


#include "gtest/gtest.h"

class ExampleTest : public ::testing::Test {
protected:

    ExampleTest() {
        testClass = Class_create();
        testRunnable = Class_getRunnable(testClass);
    }

    ~ExampleTest() override {
    }

    void SetUp() override {
    }

    void TearDown() override {
    }

    ClassHandle testClass;
    Runnable* testRunnable;
};

TEST_F(ExampleTest, example) {
    EXPECT_EQ(Class_getCount(testClass), 0);
    testRunnable->run(testRunnable);
    EXPECT_EQ(Class_getCount(testClass), 1);
    testRunnable->run(testRunnable);
    EXPECT_EQ(Class_getCount(testClass), 2);
}
